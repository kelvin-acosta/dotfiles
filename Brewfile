# Taps
tap 'caskroom/cask'
tap 'caskroom/fonts'
tap 'caskroom/versions'
tap 'homebrew/bundle'
tap 'homebrew/dupes'
tap 'homebrew/php'

# Install ZSH
brew 'zsh'
brew 'zsh-completions'

# Install GNU core utilities (those that come with macOS are outdated)
brew 'coreutils'

# Install GNU `find`, `locate`, `updatedb`, and `xargs`, g-prefixed
brew 'findutils'

# Install Bash 4
brew 'bash'

# Install more recent versions of some macOS tools
brew 'grep'

# Install Binaries
brew 'awscli'
brew 'git'
brew 'hub'
brew 'tree'
brew 'mackup'
brew 'mas'
brew 'node'
brew 'trash'
brew 'wget'
brew 'vim'
brew 'parallel'

# Development
brew 'php71'
brew 'php71-xdebug'
brew 'mysql'
brew 'go'
brew 'openssl'
brew 'node'
brew 'nvm'
brew 'rbenv'
brew 'python'
brew 'python3'
brew 'terraform'
brew 'erlang'
brew 'elixir'
brew 'yarn'

# Tools
brew 'htop'
brew 'gnu-sed'
brew 'diff-so-fancy'
brew 'httping'
brew 'kops'
brew 'kubernetes-cli'
brew 'nmap'
brew 'vim'
brew 'zplug'

# Apps
cask 'docker'
cask 'firefox'
cask 'google-chrome'
cask 'google-chrome-canary'
cask 'visual-studio-code'
cask 'java'
cask 'iterm2'
cask 'mysqlworkbench'
cask 'sequel-pro'
cask 'spotify'
cask 'slack'
cask 'tunnelbear'
cask 'postman'
cask 'vagrant'
cask 'virtualbox'
cask 'virtualbox-extension-pack'
cask 'vlc'
cask 'spectacle'

# Quicklook
cask 'qlcolorcode'
cask 'qlmarkdown'
cask 'quicklook-json'
cask 'quicklook-csv'
cask 'qlstephen'

# Fonts
cask 'font-source-code-pro-for-powerline'
cask 'font-source-code-pro'
cask 'font-inconsolata-g-for-powerline'

# Install Mac App Store apps
mas 'Xcode', id: 497799835

